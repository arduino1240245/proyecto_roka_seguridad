//Hecho por Federico Capdeville
//Probado con el Blue Pill (STM32F103C8T6)

//Defino el RTC desde la compu, por puerto Serial y lo bajo al micro.
//Luego, envio los datos a traves del Serial por si los quiero ver en la compu

//Variables que uso para el RTC (puerto serie)
/* w ==> Leo del micro y muestro en el programa de compu: año, mes, dia (fecha), hora y minutos
   r ==> Togglea el led de la bluepill (para pruebas internas mias)
   y ==> Muestra el numero de la validadora que esta anexado a la placa
   v ==> Envia el numero de la validadora a escribir desde el programa de compu
   f yyyy-mm-dd hh:mm## ==> Mando desde la compu para setear al RTC del micro, el ## indica el final del string
   l lAB## ==> Manda desde la compu el ID de la llave a guardar en la EEPROM. Ocupa 9 posiciones de memoria indicando que es una llave (una posicion) y el numero ID de esta (2 posiciones). El resto son relleno para equiparar con los eventos (6 de relleno).
   q ==> Muestra lo que hay escrito en la EEPROM en las primeras 1000 posiciones (En verdad, se fija lo que es distinto a 255 y muestra eso)
   t ==> Borra 9 posiciones especificas de la memoria EEPROM (ya que cada bloque de memoria para una llave ocupa 9 posiciones)
   
   Ejemplo de envio: f2018-11-01 16:30##    (envio por el hercules)
*/

#include <TimeLib.h>
#include <SPI.h>
#include <MFRC522.h>
#include <RTClock.h>
#include <Wire.h>



#define chipAddress 80    //Porque respeta el 101 que indica las hojas de dato en A0, A1, A2
#define LED PC13
#define RST  PB0
#define MISO PA6
#define MOSI PA7
#define SCK  PA5
#define SDA  PA4
#define SDA_I2C PB7
#define SCL_I2C PB6

//Disposicion de pines para el RFID
/*
     Disposicion de pines:
     3.3V ==> 3.3V
     RST  ==> Pin B0
     GND  ==> GND
     IRQ no se conecta
     MISO ==> Pin A6
     MOSI ==> Pin A7
     SCK  ==> Pin A5
     SS/SDA  ==> Pin A4
*/


//Variables que uso para el RTC
RTClock rtc (RTCSEL_LSI);   //Si uso el RTCSEL_LSE, asume que entre PC15 y PC14 tengo un cristal de 32.768MHz ==> Debo usar RTCSEL_LSI para hacer pruebas

uint8 seteo_amd[17]; //Seteo año, mes, dia, hora y minutos
int a = 0, i = 0;
int ano, mes, dia, hora, minuto;
uint32_t tiempo = 0;

struct tm_t time_tm;

//Variables que uso para el I2C
int num_escritura = 0;
int num_lectura = 0;
uint8 escape = 0, datos_serie = 0;
uint8 variables[4];
int contador = 0;
int centena = 0, decena = 0, unidad = 0;
byte val = 45;
byte recibo_data = 0;

//Variables que uso para lo que me envia la PC - Llaves
char memoria_recorrido = chipAddress + 1;       //Uso esto para recorrer la memoria en busca de lugar para escribir la llave o el evento
uint8 llaves[3];
/*
 * En 0: 'l'  indicando que empieza
 * En 1: Numero de llave
 * En 2: Numero de llave
 * En 3: '#'  indicando que termina
 */

//Variables que uso para la lectura / borrado de la memoria EEPROM
char memoria;
int cont = 0;
uint8 borrado[4];
int centena_borrado = 0, decena_borrado = 0, unidad_borrado = 0, numero_final = 0;
byte lectura = 0;


///MFRC522 mfrc522(SDA, RST);  //Instancio la clase


void setup()
{
  pinMode(LED, OUTPUT);      //Coloco un led en la pata PB12
  pinMode(RST, OUTPUT);

  Serial.begin(9600);
  Wire.begin();

  //Necesario para el RFID
  SPI.begin();              //Inicializo el SPI
  //  mfrc522.PCD_Init();       //Init MFRC522
}


void loop()
{
  while (Serial.available())
  {
    ///////////////////////////////////////////////////////////////////////////////////////////
    //Parte del programa referido al RTC
    ///////////////////////////////////////////////////////////////////////////////////////////

    //Cosas del RTC
    tiempo = rtc.getTime();

    //Cosas del puerto Serial
    uint8 input;
    input = Serial.read();
    uint8 escape = 0, datos_serie = 0;

    //Blanqueo un par de variables
    i = 0;
    escape = 0;
    seteo_amd[16] = 0;

    //Si se pide desde la compu los datos del RTC actuales del micro
    switch (input)
    {
      case 'w':                                               //Envio año, mes, dia (fecha) y dia de la semana
        Serial.print("\n");

        Serial.print(year(tiempo));     //Envio año
        Serial.print('-');
        Serial.print(month(tiempo));    //Envio mes
        Serial.print('-');
        Serial.print(day(tiempo));      //Envio dia
        Serial.print('-');
        Serial.print(hour(tiempo));     //Envio la hora
        Serial.print('-');
        Serial.print(minute(tiempo));   //Envio los minutos
        break;

      case 'r':                   //Toglea el led que esta en el bluepill
        digitalWrite(LED, !digitalRead(LED));
        break;

      case 'y':                         //Tocando y leo lo que se escribio en la posicion de address
        Serial.print('\n');
        Serial.print("Numero leido de la memoria:");

        Serial.println(readAddress(num_lectura), DEC);    //Lee lo que hay en la posicion de la validadora

        num_lectura = 0;
        break;
    }

    //Si envio el numero de la validadora a escribir desde la compu
    while ((input == 'v') && (escape == 0))
    {
      datos_serie = Serial.read();

      variables[contador] = datos_serie;
      contador++;

      //Convierto a ASCII
      centena = variables[0] - 48;
      decena = variables[1] - 48;
      unidad = variables[2] - 48;

      Serial.print("Contador vale: ");
      Serial.print(contador);
      Serial.print('\n');

      //Escribo por el puerto serie lo que voy a meter en la memoria
      if ((variables[3] == '#') && (contador == 4))
      {
        Serial.print("Enviado: ");
        Serial.print(centena);
        Serial.print(decena);
        Serial.print(unidad);
        Serial.print('\n');

        //Meto en la memoria los datos
        val = centena * 100 + decena * 10 + unidad;
        writeAddress(num_escritura, val);

        //Limpio las variables para no crear un "falso positivo"
        variables[0] = 0;
        variables[1] = 0;
        variables[2] = 0;
        variables[3] = 0;
        escape = 1;
        contador = 0;
      }
    }


    //Si se pide desde la compu actualizar la informacion del RTC
    while ((input == 'f') && (escape == 0))                   //Voy leyendo los datos hasta que reciba un #, indicando el final
    {
      datos_serie = Serial.read();

      seteo_amd[i] = datos_serie;
      i++;

      if (seteo_amd[16] == '#')                               //Si termino, que guarde lo enviado en el RTC
      {
        ano = (seteo_amd[0] - 48) * 1000 + (seteo_amd[1] - 48) * 100 + (seteo_amd[2] - 48) * 10 + (seteo_amd[3] - 48);
        mes = (seteo_amd[5] - 48) * 10 + (seteo_amd[6] - 48);
        dia = (seteo_amd[8] - 48) * 10 + (seteo_amd[9] - 48);
        hora = (seteo_amd[11] - 48) * 10 + (seteo_amd[12] - 48);
        minuto = (seteo_amd[14] - 48) * 10 + (seteo_amd[15] - 48);

        time_tm.year = ano - 1970;
        time_tm.month = mes;
        time_tm.day = dia;
        time_tm.pm = 0;
        time_tm.hour = hora;
        time_tm.minute = minuto;
        time_tm.second = 0;

        rtc.setTime(time_tm);

        escape = 1;                                           //Le indico que termino la cadena de elementos enviados
        i = 0;
      }
    }

    //Si se envia los datos para escribir en la memoria una llave nueva
    while ((input == 'l') && (escape == 0))                   //Voy leyendo los datos hasta que reciba un #, indicando el final
    {
      //Recibo los datos
      datos_serie = Serial.read();

      //divido los datos en un vector
      llaves[i] = datos_serie;
      i++;

      //Si llega al final del vector
      if (llaves[2] == '#')
      {        
        //Busco cual es la posicion de memoria disponible siguiente
        while ((readAddress(num_lectura+cont)) != 255)
          cont++;

        //Ahora escribo los datos de la llave que quiero escribir en la EEPROM
        writeAddress(num_escritura+cont, 'l');
        writeAddress(num_escritura+cont+1, llaves[0]);
        writeAddress(num_escritura+cont+2, llaves[1]);
        writeAddress(num_escritura+cont+3, 95);
        writeAddress(num_escritura+cont+4, 95);
        writeAddress(num_escritura+cont+5, 95);
        writeAddress(num_escritura+cont+6, 95);
        writeAddress(num_escritura+cont+7, 95);
        writeAddress(num_escritura+cont+8, 95);

        //Limpio las variables para no crear un "falso positivo"
        llaves[0] = 0;
        llaves[1] = 0;
        llaves[2] = 0;
        cont = 0;
        i = 0;
        escape = 1;
      }
    }

    //Si se quiere leer la memoria EEPROM
    while ((input == 'q') && (escape == 0))                   //Leo los datos de la memoria y los muestro
    {
      //Recibo los datos
      datos_serie = Serial.read();

      //Guardo los datos en memoria
      memoria = datos_serie;

      for (int i = 0; i < 1000; i++)
      {
        lectura = readAddress(num_lectura+i);
        if ((lectura != 255) && (lectura != 95))            //num_lectura es donde empieza, a partir de eso voy marcando lo que veo
        {
          Serial.print('\n');
          Serial.print("Diferencia en: ");
          Serial.print(num_lectura+i, DEC);
          Serial.print("   Esta escrito: ");
          Serial.print(readAddress(num_lectura+i));
        }
        if (lectura == 95)
        {
          Serial.print('\n');
          Serial.print("Hay relleno en: ");
          Serial.print(num_lectura+i);
        }
      }

      Serial.print('\n');
      Serial.println("Terminamo");
      escape = 1;
    }

    //Si se quiere borrar una posicion de la memoria EEPROM
    while ((input == 't') && (escape == 0))
    {
      //Recibo los datos
      datos_serie = Serial.read();

      //divido los datos en un vector
      borrado[i] = datos_serie;
      i++;

      //Si llega al final del vector
      if (borrado[3] == '#')
      {
        //Convierto el ASCII enviado a numero
        centena_borrado = (borrado[0] - 48) * 100;
        decena_borrado = (borrado[1] - 48) * 10;
        unidad_borrado = (borrado[2] - 48) * 1;
        numero_final = centena_borrado + decena_borrado + unidad_borrado;

        //Muestro la posicion que se va a borrar
        Serial.print('\n');
        Serial.print("Lo que borraste:");
        Serial.println(numero_final, DEC);
        Serial.print("Lo que borraste:");
        Serial.println(numero_final+1, DEC);
        Serial.print("Lo que borraste:");
        Serial.println(numero_final+2, DEC);

        //Sobreescribo esa posicion (y las 2 siguientes) con un '0'
        writeAddress(num_escritura+numero_final, 255);
        writeAddress(num_escritura+numero_final+1, 255);
        writeAddress(num_escritura+numero_final+2, 255);
        writeAddress(num_escritura+numero_final+3, 255);
        writeAddress(num_escritura+numero_final+4, 255);
        writeAddress(num_escritura+numero_final+5, 255);
        writeAddress(num_escritura+numero_final+6, 255);
        writeAddress(num_escritura+numero_final+7, 255);
        writeAddress(num_escritura+numero_final+8, 255);
        
        Serial.println("Terminamo");

        //Limpio las variables para no crear un "falso positivo"
        borrado[0] = 0;
        borrado[1] = 0;
        borrado[2] = 0;
        borrado[3] = 0;
        i = 0;
        escape = 1;      
      }
    }    
  }


  ///////////////////////////////////////////////////////////////////////////////////////////
  //Parte del programa referido al RFID
  ///////////////////////////////////////////////////////////////////////////////////////////
  /*
      if (mfrc522.PICC_IsNewCardPresent())
      {
        if (mfrc522.PICC_ReadCardSerial())
        {
          Serial.print ("Tag UID:");
          for (byte i = 0; i < mfrc522.uid.size; i++)
            {
              Serial.print (mfrc522.uid.uidByte[i] < 0x10 ? "0" : " ");
              Serial.print (mfrc522.uid.uidByte[i], HEX);
            }

        Serial.println();
        mfrc522.PICC_HaltA();
        }
      }*/
}

void writeAddress (int address, byte val)       //Funcionando para un solo bit
{
  Wire.beginTransmission(chipAddress);
  Wire.write( (int) (address >> 8));
  Wire.write( (int) (address & 0xFF));

  Wire.write(val);
  Wire.endTransmission();

  delay (20);
}

byte readAddress(int address)                   //Funcionando para un solo bit
{
  byte rData = 0;

  Wire.beginTransmission(chipAddress);
  Wire.write( (int) (address >> 8));
  Wire.write( (int) (address & 0xFF));
  Wire.endTransmission();

  Wire.requestFrom(chipAddress, 1);

  if (Wire.available())
  {
    rData = Wire.read();
  }

  return rData;
}
